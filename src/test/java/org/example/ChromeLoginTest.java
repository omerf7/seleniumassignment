package org.example;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;

import java.time.Duration;

import static java.lang.invoke.MethodHandles.lookup;
import static org.junit.jupiter.api.Assertions.*;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * automation test suit for login process
 */
public class ChromeLoginTest {

    static final Logger log = getLogger(lookup().lookupClass());
    private WebDriver driver;
    private final String URL_FOR_LOGIN = "https://test.inuka.io/#/login";
    private final String TRUE_USERNAME = "";
    private final String TRUE_PASSWORD = "";
    private final String EXPECTED_URL_AFTER_SUCCESS_LOGIN = "https://test.inuka.io/#/index-user";
    private final String WRONG_USERNAME = "example@gmail.com";
    private final String WRONG_PASSWORD = "random";

    @BeforeAll
    static void setupClass(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void shouldLoginWithTrueCredentials() {

        // given
        driver.manage().window().maximize();
        // navigate required url
        driver.get(URL_FOR_LOGIN);
        // locate elements
        WebElement emailTextElement = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(webDriver -> webDriver.findElement(By.id("email")));
        WebElement passwordTextElement = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(webDriver -> webDriver.findElement(By.id("password")));
        WebElement loginButton = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(webDriver -> webDriver.findElement(By.className("btn")));

        // when
        // perform actions
        emailTextElement.sendKeys(TRUE_USERNAME);
        passwordTextElement.sendKeys(TRUE_PASSWORD);
        loginButton.click();
        log.info("login button clicked. waiting for response");
        // wait for the redirect url
        try {
            WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofSeconds(30));
            driverWait.until(ExpectedConditions.urlToBe(EXPECTED_URL_AFTER_SUCCESS_LOGIN));
        }catch (TimeoutException e) {
            e.printStackTrace();
        }

        // then
        log.debug("Drivers current url is {}",driver.getCurrentUrl());
        assertEquals(EXPECTED_URL_AFTER_SUCCESS_LOGIN, driver.getCurrentUrl());
    }

    @Test
    void shouldNotLoginWithWrongCredentials_AndShouldShowSpanWithErrorMessage(){

        // given
        driver.manage().window().maximize();
        // navigate required url
        driver.get(URL_FOR_LOGIN);
        // locate elements
        WebElement emailTextElement = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(webDriver -> webDriver.findElement(By.id("email")));
        WebElement passwordTextElement = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(webDriver -> webDriver.findElement(By.id("password")));
        WebElement loginButton = new WebDriverWait(driver, Duration.ofSeconds(30))
                .until(webDriver -> webDriver.findElement(By.className("btn")));

        // when
        // perform actions
        emailTextElement.sendKeys(WRONG_USERNAME);
        passwordTextElement.sendKeys(WRONG_PASSWORD);
        loginButton.click();
        log.info("login button clicked. waiting for response");

        // then
        // locate error span
        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofSeconds(30));
        assertDoesNotThrow(()->{
            driverWait.until(ExpectedConditions.presenceOfElementLocated(
                    By.xpath("//span[contains(text(),'You have entered wrong email or password.')]")
            ));
        });

    }

}
